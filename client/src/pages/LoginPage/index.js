import React from 'react';
import { connect } from 'react-redux';
import { push as pushAction } from 'connected-react-router';
import { Card, Col, Layout, Row, notification } from 'antd';

import { Auth } from '../../features';

const { AuthForm, actions: authActions } = Auth;

class LoginPage extends React.Component {
  componentWillMount() {
    const { token, push } = this.props;

    if (token !== null) {
      notification.warning({
        message: 'You are already logged in.',
        description: (<p>You might want to go directly to the <a href="/">subventions page</a></p>)
      });
      if (push) {
        setTimeout(() => (push('/')), 2000);
      }
    }
   }

  render() {
    const { auth, push } = this.props;

    return (
      <Layout>
        <Row style={{ marginTop: 24, marginBottom: 24 }}>
          <Col
            xs={{ span: 24 }}
            sm={{ span: 24 }}
            md={{ span: 20, offset: 2 }}
            lg={{ span: 12, offset: 6 }}
            xl={{ span: 8, offset: 8 }}
            xxl={{ span: 6, offset: 9 }}
          >
            <Card title="Login">
              <AuthForm submit={({ username, password }) => auth(username, password, push)} />
            </Card>
          </Col>
        </Row>
      </Layout>
    );
  }
};

const mapStateToProps = ({ auth: { token } }) => ({
  token,
})

const mapDispatchToProps = ({
  auth: authActions.auth,
  push: pushAction,
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
