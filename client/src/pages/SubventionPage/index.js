import React from 'react';
import { connect } from 'react-redux';
import { Card, notification, Row, Col, Layout } from 'antd';
import { withRouter } from 'react-router-dom';
import { push as pushAction } from 'connected-react-router';

import { Auth, RefundRequest, Subventions } from '../../features';

const { LogoutButton } = Auth;
const { actions: requestActions, RefundRequestModal } = RefundRequest;
const { actions: subventionsActions, SubventionsList } = Subventions;

class SubventionPage extends React.Component {
  componentWillMount() {
    const { fetchSubventions, token, push } = this.props;

    if (token === null) {
      notification.error({
        message: 'You need to log in to access this page.',
      })
      return push('/login');
    } else {
      fetchSubventions(token);
    }
  }

  render() {
    const {
      closeRequestModal,
      isFetchingSubventions,
      isRequestModalOpen,
      openedSubvention,
      openRequestModal,
      subventions,
      token,
      postRefundRequest,
    } = this.props;

    if (token === null) {
      return (
        <Row style={{ marginTop: 24, marginBottom: 24 }}>
          <Col
            xs={{ span: 24 }}
            sm={{ span: 24 }}
            md={{ span: 20, offset: 2 }}
            lg={{ span: 12, offset: 6 }}
            xl={{ span: 8, offset: 8 }}
            xxl={{ span: 6, offset: 9 }}
          >
            <Card title='Unauthorized Access'>
              <h4>Please, <a href='/login'>log in</a>.</h4>
            </Card>
          </Col>
        </Row>
      );
    }

    return (
      <Layout>
        <Row style={{ marginTop: 24, marginBottom: 24 }}>
          <Col
            xs={{ span: 24 }}
            sm={{ span: 24 }}
            md={{ span: 24 }}
            lg={{ span: 22, offset: 1 }}
            xl={{ span: 22, offset: 1 }}
            xxl={{ span: 20, offset: 2 }}
          >
            <Card
              title={<h1>Subventions</h1>}
              extra={<LogoutButton />}
            >

              <RefundRequestModal
                close={closeRequestModal}
                isOpen={isRequestModalOpen}
                subvention={openedSubvention}
                submit={(values) => postRefundRequest(values, token)}
              />

              <SubventionsList
                subventions={subventions}
                action={openRequestModal}
                isLoading={isFetchingSubventions}
              />
            </Card>
          </Col>
        </Row>
      </Layout>
    );
  }
};

const mapStateToProps = ({ auth: { token }, refund, subventions: { subventions, isFetching: isFetchingSubventions } }) => ({
  isRequestModalOpen: refund.isOpen,
  openedSubvention: refund.subvention,
  subventions,
  isFetchingSubventions,
  token,
})

const mapDispatchToProps = ({
  closeRequestModal: requestActions.closeModal,
  openRequestModal: requestActions.openModal,
  postRefundRequest: requestActions.postRefundRequest,
  fetchSubventions: subventionsActions.fetchSubventions,
  push: pushAction,
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SubventionPage));
