import React from 'react';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import { routes, configureStore, history } from './root';
import './App.css';

const App = () => (
  <Provider store={configureStore()}>
    <ConnectedRouter history={history}>
      {renderRoutes(routes)}
    </ConnectedRouter>
  </Provider>
);

export default App;
