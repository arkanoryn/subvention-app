import React, { Component } from 'react';
import {
  Button,
  Form,
  Icon,
  Upload,
} from 'antd';

const FormItem = Form.Item;

const defaultFormItemProps = {
  hasFeedback: false,
  label: false,
};

const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

const defaultDecorator = {
  valuePropName: 'fileList',
  getValueFromEvent: (e) => { return normFile(e); },
};

const FormItemUpload = ({ id,
  title,
  buttonText = 'Upload',
  buttonIcon = 'upload',
  isLoading = false,
  customDecorator = {},
  getFieldDecorator,
  customFormItemProps = {},
  uploadProps = {},
}) => {
  const formItemProps = { ...defaultFormItemProps, ...customFormItemProps };
  const decorator = { ...defaultDecorator, customDecorator };

  return (
    <FormItem {...formItemProps}>
      {
        getFieldDecorator(id, decorator)(
          <Upload {...uploadProps}>
            {
              title &&
              <div>{title}</div>
            }
            <Button loading={isLoading}>
              <Icon type={buttonIcon} /> {buttonText}
            </Button>
          </Upload>,
        )
      }
    </FormItem>
  );
};

export default FormItemUpload;
