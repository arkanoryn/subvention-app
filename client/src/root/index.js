import history from './history';
import middlewares from './middlewares';
import createRootReducer from './rootReducer';
import routes, * as paths from './routes';
import configureStore from './store';

export {
  history,
  middlewares,
  createRootReducer,
  routes,
  configureStore,
  paths,
};
