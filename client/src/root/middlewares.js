import { routerMiddleware } from 'connected-react-router';
import logger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import history from './history';

const middlewares = [
  thunkMiddleware,
  process.env.RAILS_ENV !== 'production' && logger,
  routerMiddleware(history),
];

export default middlewares;
