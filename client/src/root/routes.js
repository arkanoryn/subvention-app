import React from 'react';
import { renderRoutes } from 'react-router-config';
import { SubventionPage, LoginPage } from '../pages';

const Root = ({ route }) => (
  <React.Fragment>
    {renderRoutes(route.routes)}
  </React.Fragment>
);

const HOME_PATH = '/';
const LOGIN_PATH = '/login';

const routes = [
  {
    component: Root,
    routes: [
      {
        path: HOME_PATH,
        exact: true,
        component: SubventionPage,
      }, {
        path: LOGIN_PATH,
        exact: true,
        component: LoginPage,
      },
    ],
  },
];

export default routes;
export {
  HOME_PATH,
};
