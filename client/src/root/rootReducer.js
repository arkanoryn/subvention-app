import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { Auth, RefundRequest, Subventions } from '../features';

const { reducer: authReducer } = Auth;
const { reducer: refundRequestReducer } = RefundRequest;
const { reducer: subventionsReducer } = Subventions;

const createRootReducer = history => combineReducers({
  router: connectRouter(history),
  auth: authReducer,
  refund: refundRequestReducer,
  subventions: subventionsReducer,
});

export default createRootReducer;
