import { applyMiddleware, compose, createStore } from 'redux';

import middlewares from './middlewares';
import createRootReducer from './rootReducer';
import history from './history';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configureStore = () => {
  const token = localStorage.getItem('token') || null;
  const initialState = {
    auth: {
      token,
    }
  };

  return (createStore(
    createRootReducer(history),
    initialState,
    composeEnhancer(applyMiddleware(...middlewares)),
  ));
};

export default configureStore;
