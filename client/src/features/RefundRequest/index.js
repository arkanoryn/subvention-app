import reducer, * as actions from './reducer';
import RefundRequestModal from './RefundRequestModal';

const refundRequest = {
  actions,
  reducer,
  RefundRequestModal,
}
export default refundRequest;
