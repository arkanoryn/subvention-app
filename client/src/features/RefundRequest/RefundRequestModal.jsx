import React from 'react';
import { connect } from 'react-redux';
import { Divider, Modal, Table } from 'antd';
import { sumBy } from 'lodash';

import RefundForm from './RefundForm';
import { fetchRefundRequests as fetchRefundRequestsAction } from './reducer';

const printPrice = (amount) => ((amount / 100).toLocaleString('fr', { style: "currency", currency: "EUR" }))

class RefundRequestModal extends React.Component {
  componentDidUpdate(prevProps) {
    const { subvention, fetchRefundRequests, token } = this.props;
    const { subvention: prevSubvention } = prevProps;

    if (subvention && subvention !== prevSubvention) {
      fetchRefundRequests(subvention, token);
    }
  }

  render() {
    const { isOpen, close, subvention, submit, refundRequests, isFetching } = this.props;
    const columns = [{
      dataIndex: 'amount_in_cents',
      key: 'amount',
      title: 'Amount',
      render: (amount) => printPrice(amount),
    }, {
      dataIndex: 'proof.url',
      key: 'proof',
      title: 'Document',
        render: (url) => <img width={272} alt="refund request proof" src={url} />,
    }];
    const totalAmount = sumBy(refundRequests, 'amount_in_cents');

    return (
      <Modal
        title={`Request refund for: [${subvention.name}]`}
        visible={isOpen}
        onCancel={close}
        footer={null}
      >
        <RefundForm subvention={subvention} submit={(values) => submit(values)} />
        {
          refundRequests &&
          <div>
            <Divider>Existing Requests</Divider>

            <Table
              bordered
              columns={columns}
              dataSource={refundRequests}
              loading={isFetching}
              rowKey={({ id }) => id}
              size="small"
            />

            <Divider>Total amount requested</Divider>

            <h3 style={{ textAlign: 'center' }}>{printPrice(totalAmount)}</h3>
          </div>
        }
      </Modal>
    );
  }
};

const mapStateToProps = ({ refund: { refundRequests, isFetching }, auth: { token } }) => ({
  refundRequests,
  token,
  isFetching,
})

const mapDispatchToProps = ({
  fetchRefundRequests: fetchRefundRequestsAction,
});

export default connect(mapStateToProps, mapDispatchToProps)(RefundRequestModal);
