import { notification } from 'antd';
import axios from 'axios';
import API from '../API';

const { API_ROOT, parseResponse, authorizationHeaders } = API;
const uri = '/api/refund_requests';

const printPrice = (amount) => ((amount / 100).toLocaleString('fr', { style: "currency", currency: "EUR" }))

const CLOSE_REFUND_REQUEST = 'REFUND_REQUEST/CLOSE_REFUND_REQUEST_MODAL';
const OPEN_REFUND_REQUEST = 'REFUND_REQUEST/OPEN_REFUND_REQUEST_MODAL';
const POST_REFUND_REQUEST_FAILURE = 'REFUND_REQUEST/POST_REFUND_REQUEST_FAILURE';
const POST_REFUND_REQUEST_REQUEST = 'REFUND_REQUEST/POST_REFUND_REQUEST_REQUEST';
const POST_REFUND_REQUEST_SUCCESS = 'REFUND_REQUEST/POST_REFUND_REQUEST_SUCCESS';
const FETCH_REFUND_REQUESTS_REQUEST = 'REFUND_REQUEST/FETCH_ALL_REQUEST';
const FETCH_REFUND_REQUESTS_SUCCESS = 'REFUND_REQUEST/FETCH_ALL_SUCCESS';
const FETCH_REFUND_REQUESTS_FAILURE = 'REFUND_REQUEST/FETCH_ALL_FAILURE';

const openModal = (subvention) => ({type: OPEN_REFUND_REQUEST, subvention})
const closeModal = () => ({type: CLOSE_REFUND_REQUEST})


const fetchRefundRequestsRequest = () => ({ type: FETCH_REFUND_REQUESTS_REQUEST });
const fetchRefundRequestsSuccess = (refundRequests) => ({type: FETCH_REFUND_REQUESTS_SUCCESS, refundRequests});
const fetchRefundRequestsFailure = (errors) => ({ type: FETCH_REFUND_REQUESTS_FAILURE, errors });

const fetchRefundRequests = ({ id: subvention_id }, token) => (dispatch => {
  dispatch(fetchRefundRequestsRequest());

  return axios
    .get(`${API_ROOT}${uri}`,
      {
        params: { subvention_id },
        headers: { ...authorizationHeaders(token) }
      })
    .then((res) => { return (parseResponse(res)); })
    .then(({ data }) => {
      dispatch(fetchRefundRequestsSuccess(data));
      return true;
     })
    .catch(errors => dispatch(fetchRefundRequestsFailure(errors)))
});

const postRefundRequestRequest = () => ({ type: POST_REFUND_REQUEST_REQUEST });

const postRefundRequestSuccess = (data) => {
  notification.success({
    message: 'Request successfully sent.',
    description: data && data.available_amount_in_cents && `Still ${printPrice(data.available_amount_in_cents)} available for this subvention.`,
  });

  return ({ type: POST_REFUND_REQUEST_SUCCESS });
};

const postRefundRequestFailure = (res) => {
  console.log('... data:', res);
  const { response: { data = {} } } = res;
  const { errors } = data;

    notification.error({
      message: 'An error occured. :(',
      description: errors && errors.toString(),
      duration: 0,
    });

  return ({
    type: POST_REFUND_REQUEST_FAILURE,
    errors,
  });
};

const postRefundRequest = ({ bill, subvention_id, amount_in_cents }, token) => (dispatch => {
  dispatch(postRefundRequestRequest);

  const formData = new FormData();

  formData.append('refund_request[proof]', Array.isArray(bill) ? bill[0].originFileObj : bill.originFileObj);
  formData.append('refund_request[amount_in_cents]', amount_in_cents * 100);
  formData.append('refund_request[subvention_id]', subvention_id);

  return axios
    .post(`${API_ROOT}${uri}`, formData, { headers: { 'Authorization': `Bearer ${token}`, 'content-type': 'multipart/form-data' } })
    .then((res) => { return (parseResponse(res)); })
    .then(({ data }) => {
        dispatch(postRefundRequestSuccess(data));
        dispatch(closeModal());
      return (true);
    })
    .catch(errors => {
      dispatch(postRefundRequestFailure(errors));
    });
})

const initialState = {
  isOpen: false,
  subvention: {},
  errors: [],
  isFetching: false,
  refundRequests: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CLOSE_REFUND_REQUEST: {
      return ({
        ...state,
        ...initialState,
      });
    }

    case OPEN_REFUND_REQUEST: {
      const { subvention } = action;

      return ({
        ...state,
        isOpen: true,
        subvention,
      });
    }

    case FETCH_REFUND_REQUESTS_REQUEST: {
      return ({
        ...state,
        isFetching: true,
      });
    }

    case FETCH_REFUND_REQUESTS_SUCCESS: {
      const { refundRequests } = action;

      return ({
        ...state,
        isFetching: false,
        refundRequests,
      });
    }

    case FETCH_REFUND_REQUESTS_FAILURE: {
      const { errors } = action;

      return ({
        ...state,
        isFetching: false,
        errors,
      });
    }

    default:
      return state;
  }
};

export default reducer;
export {
  closeModal,
  fetchRefundRequests,
  openModal,
  postRefundRequest,
}
