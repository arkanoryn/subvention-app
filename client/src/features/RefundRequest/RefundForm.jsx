import React from 'react';
import { Button, Form, Upload, InputNumber, Icon } from 'antd';

const printPrice = (amount) => ((amount / 100).toLocaleString('fr', { style: "currency", currency: "EUR" }))

const handleSubmit = (e, validateFields, id, submit) => {
  e.preventDefault();
  validateFields((err, values) => {
    if (!err) {
      submit({ subvention_id: id, ...values })
    }
  });
}

const RefundForm = ({
  form: {
    getFieldDecorator,
    getFieldError,
    isFieldTouched,
    validateFields,
  },
  subvention = {},
  submit,
}) => {
  const amountError = isFieldTouched('amount_in_cents') && getFieldError('amount_in_cents');
  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }

  return (
    <Form
      layout="vertical"
      onSubmit={(e) => { handleSubmit(e, validateFields, subvention.id, submit) }}
    >
      <Form.Item
        label="Refund amount"
        validateStatus={amountError ? 'error' : ''}
        help={amountError || ''}
      >
        {getFieldDecorator('amount_in_cents', {
          rules: [
            { required: true, message: 'Please input your amount.' },
            {
              validator: (_rule, value, callback) => {
                if (value > 0 && value <= subvention.amount_in_cents / 100) {
                  return callback()
                } else {
                  return callback(new Error())
                }
              }, message: `You can not ask for a refund greater than ${printPrice(subvention.amount_in_cents)}.` },
          ],
        })(
          <InputNumber
            min={0}
            max={subvention.amount_in_cents / 100 || 0}
            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder="amount"
            step="0.01"
            style={{ width: '100%' }}
          />
        )}
      </Form.Item>

      <Form.Item
        label="Bill"
      >
        <div className="dropbox">
          {getFieldDecorator('bill', {
            valuePropName: 'fileList',
            getValueFromEvent: (e) => (normFile(e)),
            rules: [{ required: true, message: 'Please, provide a bill with your request.' }]
          })(
            <Upload.Dragger
              beforeUpload={(_) => { return false; }}
            >
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support a single file upload only.<br />
                Accepted formats are: PDF, PNG, JPG and JPEG
                </p>
            </Upload.Dragger>
          )}
        </div>
      </Form.Item>

      <Form.Item>
        <Button type="primary" size="large" htmlType="submit" style={{ width: '100%' }}>
          Submit
          </Button>
      </Form.Item>
    </Form>
  );
};

const mapPropsToFields = () => {
  return {
    fileList: Form.createFormField({ value: [] }),
    amount_in_cents: Form.createFormField({ value: null }),
  };
};

export default Form.create({ mapPropsToFields })(RefundForm);
