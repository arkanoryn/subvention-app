import SubventionsList from './SubventionsList';
import reducer, * as actions from './reducer';

export default {
  SubventionsList,
  reducer,
  actions,
}
