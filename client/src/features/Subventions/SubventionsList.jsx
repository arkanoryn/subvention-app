import React from 'react';
import { Table, Button } from 'antd';
import moment from 'moment';

const validDate = ({ start_date: startDate, end_date: endDate }) => {
  const today = moment(new Date());

  return today.isBetween(moment(startDate), moment(endDate));
};

const ReimbourseButton = ({ item, action }) => (
  <Button
    disabled={!validDate(item)}
    onClick={() => action(item)}
    icon="form"
  >
    Ask for a refund
  </Button>
);

const printPrice = (amount) => ((amount / 100).toLocaleString('fr', { style: "currency", currency: "EUR" }) )

const columns = (action) => {
  return [{
    dataIndex: 'name',
    key: 'name',
    title: 'Name',
  }, {
    dataIndex: 'start_date',
    key: 'start_at',
    title: 'Start at',
    render: (date) => moment(date).format('LL'),
  }, {
    dataIndex: 'end_date',
    key: 'end_at',
    title: 'End at',
    render: (date) => moment(date).format('LL'),
  }, {
    key: 'amount',
    align: 'right',
    dataIndex: 'amount_in_cents',
    render: (amount) => printPrice(amount),
    title: 'Amount',
  }, {
    align: 'right',
    colSpan: 4,
    render: (_, record) => <ReimbourseButton item={record} action={(item) => action(item)} />,
  }]
};

const SubventionsList = ({ subventions, action = (() => {}), isLoading }) => (
  <Table
    bordered
    columns={columns(action)}
    dataSource={subventions}
    loading={isLoading}
    rowKey={({ id }) => id}
    size="small"
  />
)

export default SubventionsList;
