import { notification } from 'antd';
import API from '../API/';

const { authorizationHeaders } = API;

const FETCH_SUBVENTIONS_FAILURE = 'FETCH_SUBVENTIONS_FAILURE';
const FETCH_SUBVENTIONS_REQUEST = 'FETCH_SUBVENTIONS_REQUEST';
const FETCH_SUBVENTIONS_SUCCESS = 'FETCH_SUBVENTIONS_SUCCESS';

const initialState = {
  isFetching: false,
  subventions: [],
  errors: [],
};

const fetchSubventionsSuccess = (subventions) => ({
  type: FETCH_SUBVENTIONS_SUCCESS,
  subventions,
})

const fetchSubventionsFailure = (errors) => ({
  type: FETCH_SUBVENTIONS_FAILURE,
  errors,
})

const fetchSubventionsRequest = () => ({
  type: FETCH_SUBVENTIONS_REQUEST,
})

const fetchSubventions = (token) => (dispatch => {
  dispatch(fetchSubventionsRequest());

  return window.fetch('/api/subventions', {
    headers: authorizationHeaders(token),
  })
    .then(res => {
      if (res.status === 200) {
        return res.json();
      } else {
        notification.error({
          message: 'An error occured. :('
        });
        throw new Error('Unauthorized');
      }
    })
    .then(json => dispatch(fetchSubventionsSuccess(json)))
    .catch(errors => dispatch(fetchSubventionsFailure(errors)))
});


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUBVENTIONS_FAILURE: {
      const { errors } = action;

      return ({
        ...state,
        isFetching: false,
        errors,
      });
    }
    case FETCH_SUBVENTIONS_REQUEST: {
      return ({
        ...state,
        isFetching: true,
        errors: [],
      });
    }
    case FETCH_SUBVENTIONS_SUCCESS: {
      const { subventions } = action;

      return ({
        ...state,
        isFetching: false,
        subventions,
      });
    }

    default:
      return state;
  }
};

export default reducer;
export { fetchSubventions }
