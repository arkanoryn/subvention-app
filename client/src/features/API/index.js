const API_ROOT = process.env.REACT_APP_API_URL || 'http://localhost:3001';

const basicRequestHeaders = {
  'accept': 'application/json',
  'content-type': 'application/json',
};

const authorizationHeaders = (token) => ({
  ...basicRequestHeaders,
  'Authorization': `Bearer ${token}`,
});

function parseResponse(response) {
  if (response.status === 200 || response.status === 201) {
    return { data: response.data, headers: response.headers };
  }
  return Promise.reject(response);
};

const API = {
  API_ROOT,
  authorizationHeaders,
  basicRequestHeaders,
  parseResponse,
};

export default API;
