import React from 'react';
import { Button, Form, Icon, Input } from 'antd';

const handleSubmit = (e, { validateFields }, submit) => {
  e.preventDefault();
  validateFields((err, values) => {
    if (!err) {
      submit(values);
    }
  });
}

const AuthForm = ({ form, submit }) => {
  const { getFieldDecorator } = form;

  return (
    <Form onSubmit={(e) => handleSubmit(e, form, submit)} className="login-form">
      <Form.Item>
        {getFieldDecorator('username', {
          rules: [{ required: true, message: 'Please input your username!' }],
        })(
          <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('password', {
          rules: [{ required: true, message: 'Please input your Password!' }],
        })(
          <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
        )}
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
          Log in
          </Button>
      </Form.Item>
    </Form>
  );
};

export default Form.create()(AuthForm);
