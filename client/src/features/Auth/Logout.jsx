import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';
import { push as pushAction } from 'connected-react-router';
import { logout as logoutAction } from './reducer';

const LogoutButton = ({ logout, push }) => (
  <Button
    shape="round"
    onClick={() => { logout(push); }}
    type="danger"
    icon="logout"
  >
    Logout
  </Button>
);

const mapStateToProps = null;
const mapDispatchToProps = ({
  logout: logoutAction,
  push: pushAction,
})

export default connect(mapStateToProps, mapDispatchToProps)(LogoutButton);
