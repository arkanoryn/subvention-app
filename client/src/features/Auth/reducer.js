import React from 'react';
import { notification } from 'antd';
import API from '../API/';

const { basicRequestHeaders } = API;
const AUTH_FAILURE = 'AUTH_FAILURE';
const AUTH_REQUEST = 'AUTH_REQUEST';
const AUTH_SUCCESS = 'AUTH_SUCCESS';
const LOGOUT = 'AUTH/LOGOUT';

const logout = (push) => {
  localStorage.removeItem('token');

  if (push) {
    notification.success({
      message: 'Logout success!',
      description: (<p>You will be redirected to the <a href="/login">login page</a> in a moment.</p>),
    });
    setTimeout(() => (push('/login')), 1500);
  }
  return ({ type: LOGOUT });
}

const authRequest = () => ({ type: AUTH_REQUEST })

const authSuccess = (authorization, push) => {
  const token = authorization.split(" ")[1] || null

  localStorage.setItem('token', token);

  notification.success({
    message: 'Login success!',
    description: (<p>You will be redirected to the <a href="/">subvention page</a> in a moment.</p>),
  });
  if (push) {
    setTimeout(() => (push('/')), 1000);
  }

  return ({
    type: AUTH_SUCCESS,
    token
  });
}

const authFailure = (errors) => ({
  type: AUTH_FAILURE,
  errors
});

const auth = (email, password, push) => (dispatch => {
  dispatch(authRequest());
  const body = {
    user: {
      email,
      password,
    }
  };

  return window.fetch('/auth/sign_in', {
    method: 'POST',
    headers: basicRequestHeaders,
    body: JSON.stringify(body),
  })
    .then(res => {
      if (res.status === 200) {
        dispatch(authSuccess(res.headers.get('Authorization'), push));
      } else {
        notification.error({
          message: 'An error occured. :(',
          description: 'Did you use the right username/password?'
        });
        throw res;
      }
    })
    .catch(errors => dispatch(authFailure(errors)));
})

const initialState = {
  isLoading: false,
  token: null,
  errors: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_FAILURE: {
      const { errors } = action;

      return ({
        ...state,
        ...initialState,
        errors,
      });
    }
    case AUTH_SUCCESS: {
      const { token } = action;

      return ({
        ...state,
        ...initialState,
        token,
      });
    }
    case AUTH_REQUEST: {
      return ({
        ...state,
        ...initialState,
        isLoading: true,
      });
    }

    case LOGOUT: {
      return ({
        ...state,
        token: null,
      });
    }

    default:
      return state;
  }
};

export default reducer;
export {
  auth,
  logout,
}
