require 'rails_helper'

RSpec.describe RefundRequest, type: :model do
  context 'validations' do
    let(:user) { create(:user) }
    let(:subvention) { create(:subvention) }

    it 'is valid with valid attributes' do
      attrs = attributes_for(:refund_request).merge(user_id: user.id, subvention_id: subvention.id)

      expect(RefundRequest.new(attrs)).to be_valid
    end

    it 'is not valid if request amount is greater than subvention amount' do
      attrs = attributes_for(:refund_request, amount_in_cents: subvention.amount_in_cents + 1).merge(user_id: user.id, subvention_id: subvention.id)

      expect(RefundRequest.new(attrs)).not_to be_valid
    end

    it 'is not valid if today is not between subvention start and end date' do
      sub = create(:subvention, start_date: Date.tomorrow())
      attrs = attributes_for(:refund_request).merge(user_id: user.id, subvention_id: sub.id)

      expect(RefundRequest.new(attrs)).not_to be_valid
    end

    it 'is not valid if user already used all amount from the subvention' do
      usr = create(:user)
      create(:refund_request, user: usr, amount_in_cents: subvention.amount_in_cents, subvention_id: subvention.id)
      attrs = attributes_for(:refund_request).merge(user_id: usr.id, subvention_id: subvention.id)

      expect(RefundRequest.new(attrs)).not_to be_valid
    end
  end
end
