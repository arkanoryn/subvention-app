require 'rails_helper'

RSpec.describe RefundRequestsController, type: :controller do
  context 'GET #index' do
    context 'not signed in' do
      it 'return :unauthorized' do
        get :index
        expect(response.status).to eq(401)
      end
    end

    context 'signed in' do
      let(:user) { create(:user) }

      it 'return the list of refund requests for params subvention' do
        sign_in user
        get :index
        expect(response.status).to eq(200)
      end
    end
  end

  context 'POST #create' do
    context 'not signed in' do
      it 'return :unauthorized' do
        post :create
        expect(response.status).to eq(401)
      end
    end

    context 'signed in' do
      let(:user) { create(:user) }
      let(:subvention) { create(:subvention) }

      it 'return the list of refund requests for params subvention' do
        params = attributes_for(:refund_request).merge(user_id: user.id, subvention_id: subvention.id)
        sign_in user
        post :create, params: { refund_request: params }
        expect(response.status).to eq(201)
      end
    end
  end
end
