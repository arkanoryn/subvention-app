require 'faker'

FactoryBot.define do
  factory :refund_request do
    user
    subvention
    amount_in_cents { 100 }
    proof { Faker::Avatar.image }
  end
end
