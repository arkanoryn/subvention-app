require 'faker'

FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    password { ' testpassword' }
    password_confirmation { ' testpassword' }
  end
end
