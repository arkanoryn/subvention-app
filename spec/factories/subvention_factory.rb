require 'faker'

FactoryBot.define do
  factory :subvention do
    name { Faker::Superhero.name }
    amount_in_cents { 1000 }
    start_date { Date.yesterday() }
    end_date { Date.tomorrow() }
  end
end
