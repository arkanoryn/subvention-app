class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable,
      :jwt_authenticatable, jwt_revocation_strategy: Devise::JWT::RevocationStrategies::Null #, :omniauthable

  has_many :refund_requests

  def jwt_payload
    {
      user_id: id,
      email: email
    }
  end
end
