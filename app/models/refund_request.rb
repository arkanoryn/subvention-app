class RefundRequest < ApplicationRecord
  belongs_to :user
  belongs_to :subvention

  mount_uploader :proof, RefundRequestProofUploader

  validate :validate_user_did_not_reach_max_amount,
  :validate_dates

  def validate_user_did_not_reach_max_amount
    unless available_amount_in_cents - self.amount_in_cents >= 0
      errors.add(:requests_total_amount, "can not be greater than the subvention's allows (#{subvention.amount_in_cents / 100}€). You already asked for: #{user_used_amount_in_cents / 100}€.")
    end
  end

  def validate_dates
    range = subvention.start_date..subvention.end_date

    unless range === Date.today
      errors.add(:date_range, "must be between #{subvention.start_date} and #{subvention.end_date}.")
    end
  end

  def user_used_amount_in_cents
    user.refund_requests.where(subvention: subvention).sum(:amount_in_cents)
  end

  def available_amount_in_cents
    subvention.amount_in_cents - user_used_amount_in_cents
  end

end
