class SubventionsController < ApplicationController
  before_action :authenticate_user!
  # before_action :set_subvention, only: [:show, :update, :destroy]

  # GET /subventions
  def index
    @subventions = Subvention.all

    render json: @subventions
  end

  # # GET /subventions/1
  # def show
  #   render json: @subvention
  # end

  # # POST /subventions
  # def create
  #   @subvention = Subvention.new(subvention_params)

  #   if @subvention.save
  #     render json: @subvention, status: :created, location: @subvention
  #   else
  #     render json: @subvention.errors, status: :unprocessable_entity
  #   end
  # end

  # # PATCH/PUT /subventions/1
  # def update
  #   if @subvention.update(subvention_params)
  #     render json: @subvention
  #   else
  #     render json: @subvention.errors, status: :unprocessable_entity
  #   end
  # end

  # # DELETE /subventions/1
  # def destroy
  #   @subvention.destroy
  # end

  # private
  #   # Use callbacks to share common setup or constraints between actions.
  #   def set_subvention
  #     @subvention = Subvention.find(params[:id])
  #   end

  #   # Only allow a trusted parameter "white list" through.
  #   def subvention_params
  #     params.require(:subvention).permit(:name, :start_date, :end_date, :amount_in_cents)
  #   end
end
