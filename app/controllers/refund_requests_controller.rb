class RefundRequestsController < ApplicationController
  before_action :authenticate_user!
  # before_action :set_refund_request, only: [:show, :update, :destroy]

  # GET /refund_requests
  def index
    @refund_requests = RefundRequest.where(subvention_id: params[:subvention_id])

    render json: @refund_requests
  end

  # # GET /refund_requests/1
  # def show
  #   render json: @refund_request
  # end

  # POST /refund_requests
  def create
    @refund_request = RefundRequest.new(refund_request_params.merge!(user_id: current_user.id))

    if @refund_request.save
      render json: @refund_request.as_json.merge(available_amount_in_cents: @refund_request.available_amount_in_cents),
      status: :created,
      location: @refund_request
    else
      render json: { errors: @refund_request.errors.full_messages }, status: :unprocessable_entity
    end
  end

  # # PATCH/PUT /refund_requests/1
  # def update
  #   if @refund_request.update(refund_request_params)
  #     render json: @refund_request
  #   else
  #     render json: @refund_request.errors, status: :unprocessable_entity
  #   end
  # end

  # # DELETE /refund_requests/1
  # def destroy
  #   @refund_request.destroy
  # end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_refund_request
    @refund_request = RefundRequest.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def refund_request_params
    params.require(:refund_request).permit(:subvention_id, :amount_in_cents, :proof)
  end
end
