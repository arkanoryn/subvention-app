class CreateRefundRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :refund_requests do |t|
      t.references :subvention
      t.references :user
      t.integer :amount_in_cents
      t.string :proof

      t.timestamps
    end
  end
end
