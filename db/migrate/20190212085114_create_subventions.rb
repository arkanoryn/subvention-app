class CreateSubventions < ActiveRecord::Migration[5.2]
  def change
    create_table :subventions do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.integer :amount_in_cents

      t.timestamps
    end
  end
end
