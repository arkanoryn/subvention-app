# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

subventions_list = [
  {
    name: 'Subvention des activités sportives',
    start_date: '1/10/2018',
    end_date: '31/12/2018',
    amount_in_cents: 9_000
  }, {
    name: 'Subvention des activités culturelles',
    start_date: '1/09/2018',
    end_date: '31/12/2018',
    amount_in_cents: 12_000
  }, {
    name: 'Subvention rentrée scolaire',
    start_date: '15/08/2018',
    end_date: '30/09/2018',
    amount_in_cents: 6000
  }, {
    name: 'Subvention des affaires de toilettes pour chat et chien',
    start_date: '15/10/2018',
    end_date: '15/02/2019',
    amount_in_cents: 1700
  }, {
    name: 'Subvention père noël',
    start_date: '10/12/2018',
    end_date: '23/12/2019',
    amount_in_cents: 18_000
  }]

subventions_list.map! { |sub| Subvention.create(sub) }

user_names = ['alpha', 'beta', 'gamma', 'omega', 'zeus', 'ares', 'hades', 'athena']
user_names.map { |name| User.create({ email: "#{name}@clan.test", password: "#{name}-mypwd", password_confirmation: "#{name}-mypwd" }) }
