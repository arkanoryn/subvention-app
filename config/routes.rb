Rails.application.routes.draw do
  devise_for :users,
  path: :auth,
  controllers: {
    sessions: 'sessions',
  }

  scope '/api' do
    resources :subventions
    resources :refund_requests
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
